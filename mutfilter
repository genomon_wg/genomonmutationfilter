#! /usr/bin/env python

import argparse
from mutfilter.run import *


####################
# top level parser
parser = argparse.ArgumentParser( description = 'genomon_mutation_filter' )
parser.add_argument("--version", action = "version", version = "GenomonMutationFilter-0.1.0")
subparsers = parser.add_subparsers()


####################
# realignment filter 
parse_parser = subparsers.add_parser("realignment")
parse_parser.add_argument( '-t', '--target_mutation_file', help = 'mutation text', type = str, default = None, required = True )
parse_parser.add_argument( '-1', '--bam1', help = '1st bam file ( tumor )', type = str, default = None, required = True )
parse_parser.add_argument( '-2', '--bam2', help = '2nd bam file ( control )', type = str, default = None)
parse_parser.add_argument( '-o', '--output', help = 'Output text file', type = str, default = None, required = True)
parse_parser.add_argument( '-r', '--ref_genome', help = 'Reference genome', type = str, default = None , required = True)
parse_parser.add_argument( '-b', '--blat_path', type = str, default = None, required = True)
parse_parser.add_argument( '-m', '--tumor_min_mismatch', metavar = "tumor_min_mismatch", default='0', type=int)
parse_parser.add_argument( '-M', '--normal_max_mismatch', metavar = "normal_max_mismatch", default='100000', type=int)
parse_parser.add_argument( '-s', '--score_difference', metavar = "score_difference", default='5', type=int)
parse_parser.add_argument( '-w', '--window_size', metavar = "window_size", default='200', type=int)
parse_parser.add_argument( '-d', '--max_depth', metavar = "max_depth", default='5000', type=int)
parse_parser.add_argument( '--header', action="store_true", default=False,  dest='header_flag')
parse_parser.add_argument('--process_num', metavar = "process_num", default='1', type=int, help = "number of process")
parse_parser.add_argument('--chunk_size', metavar = "chunk_size", default='1', type=int, help = "chunk size")
parse_parser.set_defaults(func = run_realignment_filter)


####################
# indel filter parse
parse_parser = subparsers.add_parser("indel")
parse_parser.add_argument( '-t', '--target_mutation_file', help = 'mutation text', type = str, default = None, required = True )
parse_parser.add_argument( '-2', '--bam2', help = 'normal bam file', type = str, default = None, required = True)
parse_parser.add_argument( '-o', '--output', help = 'Output text file', type = str, default = None, required = True)
parse_parser.add_argument( '-s', '--search_length', metavar = "search_length", default='40', type=int)
parse_parser.add_argument( '-n', '--neighbor', metavar = "neighbor", default='5', type=int)
parse_parser.add_argument( '-b', '--base_qual', metavar = "base_qual_thres", default='20', type=int)
parse_parser.add_argument( '-d', '--min_depth', metavar = "min_depth", default='8', type=int)
parse_parser.add_argument( '-m', '--min_mismatch', metavar = "min_mismatch", default='100000', type=int)
parse_parser.add_argument( '-a', '--af_thres', metavar = "allele_frequency_thres", default='1', type=float)
parse_parser.add_argument( '--header', action="store_true", default=False,  dest='header_flag')
parse_parser.set_defaults(func = run_indel_filter)


####################
# breakpoint filter parse
parse_parser = subparsers.add_parser("breakpoint")
parse_parser.add_argument( '-t', '--target_mutation_file', help = 'mutation text', type = str, default = None, required = True )
parse_parser.add_argument( '-2', '--bam2', help = 'normal bam file', type = str, default = None, required = True)
parse_parser.add_argument( '-o', '--output', help = 'Output text file', type = str, default = None, required = True)
parse_parser.add_argument( '-d', '--max_depth', metavar = "max_depth", default='1000', type=int)
parse_parser.add_argument( '-c', '--min_clip_size', metavar = "min_clip_size", default='20', type=int)
parse_parser.add_argument( '-j', '--junc_num_thres', metavar = "junc_num_thres", default='0', type=int)
parse_parser.add_argument( '-m', '--mapq_thres', metavar = "mapping_quality_thres", default='10', type=int)
parse_parser.add_argument( '--header', action="store_true", default=False,  dest='header_flag')
parse_parser.set_defaults(func = run_breakpoint_filter)


####################
# simple repeat filter parse
parse_parser = subparsers.add_parser("simplerepeat")
parse_parser.add_argument( '-t', '--target_mutation_file', help = 'mutation text', type = str, default = None, required = True )
parse_parser.add_argument( '-o', '--output', help = 'Output text file', type = str, default = None, required = True)
parse_parser.add_argument( '-S', '--simple_repeat_db', help = 'simple_repeat_database', type = str, default = None, required = True)
parse_parser.add_argument('--header', action="store_true", default=False,  dest='header_flag')
parse_parser.set_defaults(func = run_simple_repeat_filter)


####################
args = parser.parse_args()
args.func(args)



